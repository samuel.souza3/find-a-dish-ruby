# frozen_string_literal: true

Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'

  scope :api, constraints: AcceptJsonConstraint.new do
    resources :recipes, only: %i[create index show]
  end
end
