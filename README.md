# Find a Dish

## System dependencies

The application was developed using the following tools. Other versions might work, but weren't tested:
- Ruby 3.0.4
- PostgreSQL 14.3
- ElasticSeach 8.2.2 (optional)
  - ElasticSearch also depends on a working JRE instalation
- Docker (recommended)

## Configuration

In order to ensure everyone's environment runs similarly we set Docker up to run the application and necessary services.

You can start it by running from the repository root directory:

- `docker-compose up` - This will start:
  - The rails web server listening on port 3000
  - The PostgreSQL database listening on port 5432
  - The ElasticSeach service listening on post 9200
  - Make sure all those ports aren't in use by anything else on your machine
  - This will also ensure the `development` and `test` are created

## Running commands inside the container

There's a helper script you can use to run commands inside the running Rails container

- `./scripts/run <command to run>` - This is useful, e.g., to:
  - Install dependencies: `./scripts/run bundle install`
  - Run generators: `./scripts/run rails generate model Pet name:string`
  - Run specs: `./scripts/run bundle exec rspec`
  - any other commands you'd run on the root of your rails application
