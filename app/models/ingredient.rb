# frozen_string_literal: true

class Ingredient < ApplicationRecord
  belongs_to :recipe

  validates :name, presence: true
  validates :amount,
            presence: true,
            numericality: { greater_than_or_equal_to: 0 }

  enum unit: {
    unit: 0,
    gram: 10,
    miligram: 11,
    kilogram: 12,
    cup: 20,
    spoon: 21,
    liter: 30,
    mililiter: 31
  }
end
