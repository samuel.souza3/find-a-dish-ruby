# frozen_string_literal: true

class Recipe < ApplicationRecord
  has_many :ingredients, dependent: :destroy
  has_many :steps, dependent: :destroy

  validates :name, presence: true
  validates :preparation_time,
            presence: true,
            numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :steps, presence: true
  validates :ingredients, presence: true

  validates_associated :ingredients
  validates_associated :steps

  validates_with RecipeStepsPosition
end
