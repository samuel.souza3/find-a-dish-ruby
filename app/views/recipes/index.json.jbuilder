# frozen_string_literal: true

json.data @recipes do |recipe|
  json.id recipe.id
  json.name recipe.name
end
