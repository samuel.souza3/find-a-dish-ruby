# frozen_string_literal: true

json.id recipe.id
json.name recipe.name
json.preparationTime recipe.preparation_time

json.steps recipe.steps do |step|
  json.position step.position
  json.description step.description
end

json.ingredients @recipe.ingredients do |ingredient|
  json.name ingredient.name
  json.amount ingredient.amount.to_f
  json.unit ingredient.unit
end
