# frozen_string_literal: true

json.data do
  json.partial! 'recipe', recipe: @recipe
end
