# frozen_string_literal: true

json.data do
  json.partial! 'recipes/recipe', recipe: @recipe
end
