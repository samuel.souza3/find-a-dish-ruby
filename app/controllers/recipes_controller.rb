# frozen_string_literal: true

class RecipesController < ApplicationController
  def create
    @recipe = Recipes::Creator.new.perform(**create_params)
    render :create, status: :created
  end

  def index
    @recipes = Recipes::List.new.perform
  end

  def show
    @recipe = Recipes::Loader.new.perform(**show_params)
  end

  private

  def create_params
    create_params = params.permit(
      :name,
      :preparationTime,
      steps: %i[position description],
      ingredients: %i[name amount unit]
    ).to_h
    JsonNormalizer.new.normalize(create_params)
  end

  def show_params
    show_params = params.permit(:id).to_h
    JsonNormalizer.new.normalize(show_params)
  end
end
