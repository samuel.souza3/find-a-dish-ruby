# frozen_string_literal: true

module Recipes
  class Loader
    def perform(id: nil)
      Recipe.find(id)
    end
  end
end
