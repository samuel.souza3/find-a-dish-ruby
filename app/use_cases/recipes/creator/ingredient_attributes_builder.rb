# frozen_string_literal: true

module Recipes
  class Creator
    class UnknownUnitError < StandardError; end

    class IngredientAttributesBuilder
      UNITS_MAPPING = {
        /^g(rama?s?)?$/i => :gram,
        /^(k(ilo)?)g(rama?s?)?$/i => :kilogram,
        /^kilo$/i => :kilogram,
        /^(m(ili)?)g(rama?s?)?$/i => :miligram,
        /^(copo|cup|x[íi]c(ara)?)s?$/i => :cup,
        /^(colhere?|spoon)s?$/i => :spoon,
        /^l(itro|iter)?s?$/i => :liter,
        /^(m(ili)?)l(itro|iter)?s?$/i => :mililiter
      }.freeze

      def build(attributes)
        attributes.merge(unit: convert_unit(attributes[:unit]))
      end

      private

      def convert_unit(unit)
        return :unit if unit.blank?

        UNITS_MAPPING.find { |re, _| re =~ unit }.last
      rescue NoMethodError => _e
        raise UnknownUnitError, "Unit unknown: #{unit}"
      end
    end
  end
end
