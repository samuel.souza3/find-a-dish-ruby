# frozen_string_literal: true

module Recipes
  class Creator
    def perform(name: nil, preparation_time: nil, steps: [], ingredients: [])
      Recipe.create!(
        name: name,
        preparation_time: preparation_time,
        ingredients: build_ingredients(ingredients),
        steps: build_steps(steps)
      )
    end

    private

    def build_ingredients(ingredients)
      attributes_builder = IngredientAttributesBuilder.new
      ingredients.map { |ingredient| Ingredient.new(**attributes_builder.build(ingredient)) }
    end

    def build_steps(steps)
      steps.map { |step| Step.new(**step) }
    end
  end
end
