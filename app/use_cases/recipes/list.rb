# frozen_string_literal: true

module Recipes
  class List
    def perform
      ::Recipe.all.to_a
    end
  end
end
