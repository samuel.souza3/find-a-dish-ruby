# frozen_string_literal: true

class RecipeStepsPosition < ActiveModel::Validator
  def validate(record)
    positions = record.steps.map(&:position).sort
    record.errors.add :steps, 'needs to start at 1' unless starts_at_one?(positions)
    record.errors.add :steps, "can't have any gaps" unless consecutive_steps?(positions)
  end

  private

  def consecutive_steps?(positions)
    positions.each_cons(2).all? { |a, b| b == a + 1 }
  end

  def starts_at_one?(positions)
    positions.first == 1
  end
end
