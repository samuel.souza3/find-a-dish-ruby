# frozen_string_literal: true

class CreateIngredients < ActiveRecord::Migration[7.0]
  def change
    create_table :ingredients do |t|
      t.decimal :amount, precision: 6, scale: 2
      t.integer :unit
      t.string :name
      t.belongs_to :recipe, null: false, foreign_key: true

      t.timestamps
    end
  end
end
