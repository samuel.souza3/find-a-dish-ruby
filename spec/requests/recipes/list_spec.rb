# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recipes', type: :request do
  describe 'GET /api/recipes' do
    context 'when there are no recipes' do
      let(:expected_body) do
        []
      end

      it 'is successful' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:ok)
      end

      it 'returns an empty array' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:data)

        expect(body).to eq(expected_body)
      end
    end

    context 'when there is one recipe' do
      let(:expected_body) do
        [
          { id: shrimp_risotto.id, name: 'Shrimp Risotto' }
        ]
      end

      let!(:shrimp_risotto) do
        Recipe.create(
          name: 'Shrimp Risotto',
          preparation_time: 42,
          steps: [
            Step.new(position: 1, description: 'An array'),
            Step.new(position: 2, description: 'with each step')
          ],
          ingredients: [
            Ingredient.new(name: 'An array', amount: 1, unit: :gram),
            Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
          ]
        )
      end

      it 'is successful' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:ok)
      end

      it 'returns an empty array' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:data)

        expect(body).to eq(expected_body)
      end
    end

    context 'when there are multiple recipes' do
      let(:expected_body) do
        all_recipes.map do |recipe|
          { id: recipe.id, name: recipe.name }
        end
      end

      let!(:all_recipes) do
        (1..100).map do |index|
          Recipe.create(
            name: "Recipe #{index}",
            preparation_time: 60,
            steps: [
              Step.new(position: 1, description: "step 1 recipe #{index}"),
              Step.new(position: 2, description: "step 2 recipe #{index}")
            ],
            ingredients: [
              Ingredient.new(name: "ingredient 1 recipe #{index}", amount: 1, unit: :gram),
              Ingredient.new(name: "ingredient 2 recipe #{index}", amount: 2, unit: :liter)
            ]
          )
        end
      end

      it 'is successful' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:ok)
      end

      it 'returns an empty array' do
        get '/api/recipes', headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:data)

        expect(body).to eq(expected_body)
      end
    end
  end
end
