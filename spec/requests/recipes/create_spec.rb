# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recipes', type: :request do
  describe 'POST /api/recipes' do
    context 'when params are valid' do
      let(:params) do
        {
          name: 'Shrimp Risotto',
          steps: [
            { position: 1, description: 'An array' },
            { position: 2, description: 'with each step' }
          ],
          preparationTime: 42,
          ingredients: [
            { name: 'An array', amount: 1, unit: 'g' },
            { name: 'with each ingredient name', amount: 2, unit: 'l' }
          ]
        }
      end

      let(:expected_body) do
        {
          id: Integer,
          name: 'Shrimp Risotto',
          steps: [
            { position: 1, description: 'An array' },
            { position: 2, description: 'with each step' }
          ],
          preparationTime: 42,
          ingredients: [
            { name: 'An array', amount: 1, unit: 'gram' },
            { name: 'with each ingredient name', amount: 2, unit: 'liter' }
          ]
        }
      end

      it 'is successful' do
        post '/api/recipes', params: params, headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:success)
      end

      it 'creates a new recipe' do
        expect { post '/api/recipes', params: params, headers: { 'Accept' => 'application/json' } }
          .to change { Recipe.where(name: 'Shrimp Risotto').count }
          .by(1)
      end

      it 'returns the created recipe' do
        post '/api/recipes', params: params, headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:data)

        expect(body).to include(expected_body)
      end
    end

    context 'when params are invalid' do
      let(:params) do
        {
          name: 'Shrimp Risotto',
          steps: [],
          preparationTime: nil,
          ingredients: [
            { name: 'An array', amount: 1, unit: 'g' },
            { name: 'with each ingredient name', amount: 2, unit: 'l' }
          ]
        }
      end

      let(:expected_body) do
        {
          type: 'ValidationError',
          message: 'Validation error',
          details: include(
            "Preparation time can't be blank",
            'Preparation time is not a number',
            "Steps can't be blank",
            'Steps needs to start at 1'
          )
        }
      end

      it 'is not successful' do
        post '/api/recipes', params: params, headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:conflict)
      end

      it 'returns the validation messages' do
        post '/api/recipes', params: params, headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:error)

        expect(body).to include(expected_body)
      end
    end
  end
end
