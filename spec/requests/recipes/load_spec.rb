# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Recipes', type: :request do
  describe 'GET /api/recipes/<id>' do
    context 'when the recipe does NOT exist' do
      let(:expected_body) do
        {
          type: 'NotFoundError',
          message: 'Resource not found'
        }
      end

      it 'is not successful' do
        get '/api/recipes/1', headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:not_found)
      end

      it 'returns an empty array' do
        get '/api/recipes/1', headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:error)

        expect(body).to eq(expected_body)
      end
    end

    context 'when the recipe exists' do
      let(:expected_body) do
        {
          id: shrimp_risotto.id,
          name: 'Shrimp Risotto',
          steps: [
            { position: 1, description: 'An array' },
            { position: 2, description: 'with each step' }
          ],
          preparationTime: 42,
          ingredients: [
            { name: 'An array', amount: 1, unit: 'gram' },
            { name: 'with each ingredient name', amount: 2, unit: 'liter' }
          ]
        }
      end

      let!(:shrimp_risotto) do
        Recipe.create(
          name: 'Shrimp Risotto',
          preparation_time: 42,
          steps: [
            Step.new(position: 1, description: 'An array'),
            Step.new(position: 2, description: 'with each step')
          ],
          ingredients: [
            Ingredient.new(name: 'An array', amount: 1, unit: :gram),
            Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
          ]
        )
      end

      it 'is successful' do
        get "/api/recipes/#{shrimp_risotto.id}", headers: { 'Accept' => 'application/json' }

        expect(response).to have_http_status(:ok)
      end

      it 'returns the recipe data' do
        get "/api/recipes/#{shrimp_risotto.id}", headers: { 'Accept' => 'application/json' }

        body = JSON.parse(response.body, symbolize_names: true).fetch(:data)

        expect(body).to eq(expected_body)
      end
    end
  end
end
