# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ingredient, type: :model do
  it { is_expected.to belong_to(:recipe) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:amount) }

  it { is_expected.to validate_numericality_of(:amount).is_greater_than_or_equal_to(0) }

  it { is_expected.to define_enum_for(:unit).with_values(unit: 0, gram: 10, miligram: 11, kilogram: 12, cup: 20, spoon: 21, liter: 30, mililiter: 31) }
end
