# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipe, type: :model do
  it { is_expected.to have_many(:ingredients).dependent(:destroy) }
  it { is_expected.to have_many(:steps).dependent(:destroy) }

  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_presence_of(:preparation_time) }
  it { is_expected.to validate_numericality_of(:preparation_time).only_integer }
  it { is_expected.to validate_numericality_of(:preparation_time).is_greater_than_or_equal_to(0) }

  it { is_expected.to validate_presence_of(:steps) }
  it { is_expected.to validate_presence_of(:ingredients) }

  describe 'steps position validation' do
    let(:recipe) do
      described_class.new(name: "Fish 'n Chips", preparation_time: 35, steps: steps, ingredients: ingredients)
    end

    let(:ingredients) do
      [
        Ingredient.new(name: 'Fish', amount: 1, unit: :kilogram),
        Ingredient.new(name: 'Potatoes', amount: 500, unit: :gram)
      ]
    end

    before { recipe.valid? }

    context 'when steps position start at 1 and have no gap' do
      let(:steps) do
        [
          Step.new(position: 2, description: 'Scale the fish'),
          Step.new(position: 3, description: 'Cut the potatoes in thin slices'),
          Step.new(position: 1, description: 'Peel the potatoes')
        ]
      end

      it 'accepts steps position in a sequence' do
        expect(recipe).to be_valid
      end
    end

    context "when steps position don't start at 1" do
      let(:steps) do
        [
          Step.new(position: 3, description: 'Scale the fish'),
          Step.new(position: 4, description: 'Cut the potatoes in thin slices'),
          Step.new(position: 2, description: 'Peel the potatoes')
        ]
      end

      it 'rejects the recipe' do
        expect(recipe).not_to be_valid
      end

      it 'adds an error' do
        expect(recipe.errors[:steps]).to include('needs to start at 1')
      end
    end

    context 'when steps positions have no gaps' do
      let(:steps) do
        [
          Step.new(position: 3, description: 'Scale the fish'),
          Step.new(position: 4, description: 'Cut the potatoes in thin slices'),
          Step.new(position: 1, description: 'Peel the potatoes')
        ]
      end

      it 'rejects the recipe' do
        expect(recipe).not_to be_valid
      end

      it 'adds an error' do
        expect(recipe.errors[:steps]).to include("can't have any gaps")
      end
    end
  end
end
