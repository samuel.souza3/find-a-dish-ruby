# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Step, type: :model do
  it { is_expected.to belong_to(:recipe) }

  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:position) }
  it { is_expected.to validate_numericality_of(:position).only_integer }
  it { is_expected.to validate_numericality_of(:position).is_greater_than_or_equal_to(1) }
end
