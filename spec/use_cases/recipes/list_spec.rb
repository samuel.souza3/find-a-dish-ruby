# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipes::List do
  subject(:recipes) { described_class.new.perform }

  context 'when there are no recipes' do
    it 'returns an empty array' do
      expect(recipes).to be_a(Array) & be_empty
    end
  end

  context 'when there is one recipe' do
    let!(:shrimp_risotto) do
      Recipe.create(
        name: 'Shrimp Risotto',
        preparation_time: 42,
        steps: [
          Step.new(position: 1, description: 'An array'),
          Step.new(position: 2, description: 'with each step')
        ],
        ingredients: [
          Ingredient.new(name: 'An array', amount: 1, unit: :gram),
          Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
        ]
      )
    end

    it 'return an array with the recipe' do
      expect(recipes).to include(shrimp_risotto)
    end
  end

  context 'when there are multiple recipes' do
    let(:expected_body) do
      all_recipes.map do |recipe|
        { id: recipe.id, name: recipe.name }
      end
    end

    let!(:all_recipes) do
      (1..100).map do |index|
        Recipe.create(
          name: "Recipe #{index}",
          preparation_time: 60,
          steps: [
            Step.new(position: 1, description: "step 1 recipe #{index}"),
            Step.new(position: 2, description: "step 2 recipe #{index}")
          ],
          ingredients: [
            Ingredient.new(name: "ingredient 1 recipe #{index}", amount: 1, unit: :gram),
            Ingredient.new(name: "ingredient 2 recipe #{index}", amount: 2, unit: :liter)
          ]
        )
      end
    end

    it 'returns all recipes' do
      expect(recipes).to match_array(all_recipes)
    end
  end
end
