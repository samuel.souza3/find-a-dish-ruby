# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipes::Creator do
  subject(:perform) { described_class.new.perform(**attributes) }

  context 'with valid attributes' do
    let(:attributes) do
      {
        name: 'Shrimp Risotto',
        steps: [
          { position: 1, description: 'Peel the shrimps' },
          { position: 2, description: 'Simmer the shrimps with olive oil' }
        ],
        preparation_time: 42,
        ingredients: [
          { name: 'shrimps', amount: 1, unit: 'kg' },
          { name: 'rice', amount: 500, unit: 'g' }
        ]
      }
    end

    it 'creates a new recipe' do
      expect { perform }
        .to change { Recipe.where(name: 'Shrimp Risotto').count }
        .by(1)
    end
  end

  context 'with a required field missing' do
    let(:attributes) do
      {
        name: 'Shrimp Risotto',
        steps: [
          { position: 1, description: 'Peel the shrimps' },
          { position: 2, description: 'Simmer the shrimps with olive oil' }
        ],
        ingredients: [
          { name: 'shrimps', amount: 1, unit: 'kg' },
          { name: 'rice', amount: 500, unit: 'g' }
        ]
      }
    end

    it 'raises an exception' do
      expect { perform }
        .to raise_exception(
          ActiveRecord::RecordInvalid,
          "Validation failed: Preparation time can't be blank, Preparation time is not a number"
        )
    end
  end

  context 'with an associated value missing' do
    let(:attributes) do
      {
        name: 'Shrimp Risotto',
        steps: [
          { position: 1, description: 'Peel the shrimps' },
          { position: 2, description: 'Simmer the shrimps with olive oil' }
        ],
        preparation_time: 42,
        ingredients: []
      }
    end

    it 'raises an exception' do
      expect { perform }
        .to raise_exception(
          ActiveRecord::RecordInvalid,
          "Validation failed: Ingredients can't be blank"
        )
    end
  end

  context 'with an associated value invalid' do
    let(:attributes) do
      {
        name: 'Shrimp Risotto',
        steps: [
          { position: 0, description: 'Peel the shrimps' },
          { position: 2, description: 'Simmer the shrimps with olive oil' }
        ],
        preparation_time: 42,
        ingredients: [
          { name: 'shrimps', amount: 1, unit: 'kg' },
          { name: 'rice', amount: 500, unit: 'g' }
        ]
      }
    end

    it 'raises an exception' do
      expect { perform }
        .to raise_exception(
          ActiveRecord::RecordInvalid,
          "Validation failed: Steps is invalid, Steps is invalid, Steps needs to start at 1, Steps can't have any gaps"
        )
    end
  end
end
