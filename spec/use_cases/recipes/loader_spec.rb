# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipes::Loader do
  subject(:perform) { described_class.new.perform(**attributes) }

  context 'when recipe does not exist' do
    let(:attributes) do
      { id: 1 }
    end

    it 'creates a new recipe' do
      expect { perform }
        .to raise_exception(ActiveRecord::RecordNotFound)
    end
  end

  context 'when the recipe exists' do
    let(:attributes) do
      { id: shrimp_risotto.id }
    end

    let!(:shrimp_risotto) do
      Recipe.create(
        name: 'Shrimp Risotto',
        preparation_time: 42,
        steps: [
          Step.new(position: 1, description: 'An array'),
          Step.new(position: 2, description: 'with each step')
        ],
        ingredients: [
          Ingredient.new(name: 'An array', amount: 1, unit: :gram),
          Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
        ]
      )
    end

    it 'returns the recipe' do
      expect(perform).to eq(shrimp_risotto)
    end
  end
end
