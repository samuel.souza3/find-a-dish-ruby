# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Recipes::Creator::IngredientAttributesBuilder do
  subject(:build) { described_class.new.build(attributes) }

  context 'when invalid unit is provided' do
    let(:attributes) do
      { name: 'pepper', amount: 2, unit: 'oz' }
    end

    it 'raises an error' do
      expect { build }
        .to raise_exception(Recipes::Creator::UnknownUnitError)
    end
  end

  context 'when unit is not provided' do
    let(:attributes) do
      { name: 'chicken egg', amount: 1, unit: nil }
    end

    it 'defaults to unit' do
      expect(build).to include(
        name: 'chicken egg',
        amount: 1.0,
        unit: :unit
      )
    end
  end

  %w[g gram grams grama gramas].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'wheat flour', amount: 100, unit: unit }
      end

      it 'sets it to gram' do
        expect(build).to include(
          name: 'wheat flour',
          amount: 100.0,
          unit: :gram
        )
      end
    end
  end

  %w[kg kilogram kilograms kilogramas kilograma kilo].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'ground meat', amount: 1, unit: unit }
      end

      it 'sets it to kilogram' do
        expect(build).to include(
          name: 'ground meat',
          amount: 1.0,
          unit: :kilogram
        )
      end
    end
  end

  %w[mg miligram miligrama miligrams miligramas].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'salt', amount: 500, unit: unit }
      end

      it 'sets it to miligram' do
        expect(build).to include(
          name: 'salt',
          amount: 500,
          unit: :miligram
        )
      end
    end
  end

  %w[copo cup copos cups xicara xicaras xícara xícaras].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'sugar', amount: 2, unit: unit }
      end

      it 'sets it to cup' do
        expect(build).to include(
          name: 'sugar',
          amount: 2,
          unit: :cup
        )
      end
    end
  end

  %w[colher colheres spoon spoons].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'sugar', amount: 2, unit: unit }
      end

      it 'sets it to spoon' do
        expect(build).to include(
          name: 'sugar',
          amount: 2,
          unit: :spoon
        )
      end
    end
  end

  %w[l L litro litros liter liters].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'boiling water', amount: 1, unit: unit }
      end

      it 'sets it to liter' do
        expect(build).to include(
          name: 'boiling water',
          amount: 1.0,
          unit: :liter
        )
      end
    end
  end

  %w[ml mililitro mililitros mililiter mililiters].each do |unit|
    context "when unit is #{unit}" do
      let(:attributes) do
        { name: 'milk', amount: 350, unit: unit }
      end

      it 'sets it to mililiter' do
        expect(build).to include(
          name: 'milk',
          amount: 350,
          unit: :mililiter
        )
      end
    end
  end
end
