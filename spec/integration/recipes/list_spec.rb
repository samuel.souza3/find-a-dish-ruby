# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Recipes', type: :request do
  path '/api/recipes' do
    get 'List recipes' do
      tags 'Recipes'
      consumes 'application/json'
      produces 'application/json'

      context 'when there is one recipe' do
        let!(:shrimp_risotto) do
          Recipe.create(
            name: 'Shrimp Risotto',
            preparation_time: 42,
            steps: [
              Step.new(position: 1, description: 'An array'),
              Step.new(position: 2, description: 'with each step')
            ],
            ingredients: [
              Ingredient.new(name: 'An array', amount: 1, unit: :gram),
              Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
            ]
          )
        end

        response '200', 'List of recipes' do
          examples 'application/json' => { data: [{ id: 1, name: 'Shrimp Risotto' }] }

          run_test!
        end
      end
    end
  end
end
