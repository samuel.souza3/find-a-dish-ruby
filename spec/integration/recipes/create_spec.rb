# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Recipes', type: :request do
  path '/api/recipes' do
    post 'Creates a recipe' do
      tags 'Recipes'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :recipe, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          preparationTime: { type: :integer },
          steps: {
            type: :array,
            items: {
              type: :object,
              properties: {
                description: { type: :string },
                position: { type: :integer }
              }
            }
          },
          ingredients: {
            type: :array,
            items: {
              type: :object,
              properties: {
                name: { type: :string },
                amount: { type: :number },
                unit: { type: :string, enum: Ingredient.units.keys }
              }
            }
          }
        },
        required: %w[name preparationTime steps ingredients]
      }

      context 'when params are valid' do
        response '201', 'Recipe created' do
          examples 'application/json' => {
            data: {
              id: 36,
              name: 'Shrimp Risotto',
              steps: [
                { position: 1, description: 'An array' },
                { position: 2, description: 'with each step' }
              ],
              preparationTime: 42,
              ingredients: [
                { name: 'An array', amount: 1, unit: 'gram' },
                { name: 'with each ingredient name', amount: 2, unit: 'liter' }
              ]
            }
          }

          let(:recipe) do
            {
              name: 'Shrimp Risotto',
              steps: [
                { position: 1, description: 'An array' },
                { position: 2, description: 'with each step' }
              ],
              preparationTime: 42,
              ingredients: [
                { name: 'An array', amount: 1, unit: 'g' },
                { name: 'with each ingredient name', amount: 2, unit: 'l' }
              ]
            }
          end

          run_test!
        end
      end

      context 'when params are invalid' do
        response '409', 'Validation failed' do
          examples 'application/json' => {
            error: {
              type: 'ValidationError',
              message: 'Validation error',
              details: [
                "Preparation time can't be blank",
                'Preparation time is not a number',
                "Steps can't be blank",
                'Steps needs to start at 1'
              ]
            }
          }

          let(:recipe) do
            {
              name: 'Shrimp Risotto',
              steps: [],
              preparationTime: 42,
              ingredients: [
                { name: 'An array', amount: 1, unit: 'g' },
                { name: 'with each ingredient name', amount: 2, unit: 'l' }
              ]
            }
          end

          run_test!
        end
      end
    end
  end
end
