# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Recipes', type: :request do
  path '/api/recipes/{id}' do
    get 'List recipes' do
      tags 'Recipes'
      consumes 'application/json'
      produces 'application/json'

      parameter name: :id, in: :path, schema: { type: :integer }

      context 'when the recipe does NOT exist' do
        let(:id) { 99 }

        response '404', 'Not found error' do
          examples 'application/json' => {
            error: [
              {
                type: 'NotFoundError',
                message: 'Resource not found'
              }
            ]
          }

          run_test!
        end
      end

      context 'when the recipe exists' do
        let!(:shrimp_risotto) do
          Recipe.create(
            name: 'Shrimp Risotto',
            preparation_time: 42,
            steps: [
              Step.new(position: 1, description: 'An array'),
              Step.new(position: 2, description: 'with each step')
            ],
            ingredients: [
              Ingredient.new(name: 'An array', amount: 1, unit: :gram),
              Ingredient.new(name: 'with each ingredient name', amount: 2, unit: :liter)
            ]
          )
        end

        let(:id) { shrimp_risotto.id }

        response '200', 'Load the Recipe' do
          examples 'application/json' => {
            data: [
              {
                id: 1,
                name: 'Shrimp Risotto',
                steps: [
                  { position: 1, description: 'An array' },
                  { position: 2, description: 'with each step' }
                ],
                preparationTime: 42,
                ingredients: [
                  { name: 'An array', amount: 1, unit: 'gram' },
                  { name: 'with each ingredient name', amount: 2, unit: 'liter' }
                ]
              }
            ]
          }

          run_test!
        end
      end
    end
  end
end
